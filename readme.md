# :warning: Deprecated! :warning:

I quickly hacked this project to get more out of my notes, using technology that I was familiar with. Particularly important for me was future compatibility: I didn't want to get locked into a product with proprietary formats/databases.

Several projects are now doing what I wanted in a smoother, more feature-ful way than I achieved using the emergent "markdown + #hashtags + [[wikilinks]] standard", such as
- [Logseq](https://logseq.com/) (FLOSS)
- [Obsidian](https://obsidian.md/) (proprietary)
- [Joplin](https://joplinapp.org/) (FLOSS) 
- [Roam Research](https://roamresearch.com/) (proprietary) 

---

# Noteasy
_Pronunciation: note-easy... not-easy... you decide_

A tool for organized viewing of local markdown notes in the browser via [file URIs](https://en.wikipedia.org/wiki/File_URI_scheme) for easy editing. Built using [Metalsmith](https://metalsmith.io/) (_"An extremely simple, pluggable static site generator."_) with [Nunjucks](https://mozilla.github.io/nunjucks/) for templating.

![](assets/noteasyDemo.webm)

### 'Quick' start
**Note:** project assumes you use linux (debian-based, to be precise), firefox and have `npm`, `node` and `git` installed.

Install project and dependencies, build then open
```shell
$ git clone https://gitlab.com/DougInAMug/noteasy.git
$ cd noteasy
$ npm install
$ node build
$ firefox --new-tab build/index.html
```

To include your own markdown notes in the build process, symlink containing directories to `source`
```shell
$ ln -s /path/to/your/md/notes source/symlinkToNotesDirectory
```

Autobuild when changes are made using `nodemon`
```shell
$ npm install -g nodemon
$ nodemon -e md,njk,js --watch /path/to/noteasy /path/to/noteasy/build.js
```

Autorefresh when build is finished. In order for this to work you need to have noteasy pinned as your first tab! A bit hacky, but works for me
```shell
$ sudo apt install xdotool
$ chmod 777 path/to/noteasy/assets/refreshFirstPinnedFirefoxTab.sh
```
---

### Background
I write notes. Quite a lot of them actually. I used to write in just plaintext, but was very happy when markdown came around. I was then even happier when YAML frontmatter popped up. Now I write my notes exclusively in this YFMMD format, sometimes with a bit of extra html, and this seems to be a common 'standard' for most SSGs and more.

Going from .txt to .md doesn't help you with finding notes though. Trying to organize with strict file name or folder protocols never seems to really work for me, just feels like trying to hold sand...

Some time in early 2019 I was introduced to [Metalsmith](https://metalsmith.io/) by a friend and I used it to hack together my [website](https://dougwebb.site/). I immediately thought about using it for my markdown notes: with all the plugins it would be simple to create a nice web interface which makes use of all the metadata. Tags, tags, tags! (And types, and collections, etc) 

Another Metalsmith user linked me to a similar project they whipped up, [keeping-my-notes](https://gitlab.com/ItsMeBender/keeping-my-notes). After playing around with it, I realized it's not possible to access local files through a server! This prevents you from easily editing local files, which from a security perspective is generally A Good Thing, but not in my case.

Noteasy was built around `file://` in order to really keep the project as a visualizer/organizer for notes, while easily linking back to them.

---

2019-10-29
- finally put in some icons!
- now using nunjucks 'extend' to keep boilerplate html in 'base.njk' 
- used nunjucks to set 'base' URL (file:///home/doug/dev/yamp) in base.njk, got rid of metalsmith-rootpath

2020-03-05
- after installing `.gitignore` to remove my personal notes, finally uploaded to Gitlab

