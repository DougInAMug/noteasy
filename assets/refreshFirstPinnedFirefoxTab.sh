#!/bin/bash

aFirefoxID="$(xdotool search --desktop 0 --name '\<Firefox\>' | head -n 1)"
xdotool key --window "${aFirefoxID}" alt+1
xdotool key --window "${aFirefoxID}" ctrl+r
