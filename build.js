#!/usr/bin/env node

'use strict';

const Metalsmith    = require('metalsmith')
const debug         = require('metalsmith-debug')
const collections   = require('metalsmith-collections')
const inplace       = require('metalsmith-in-place')
const layouts       = require('metalsmith-layouts')
const tags          = require('metalsmith-tags')
const writemetadata = require('metalsmith-writemetadata')
const paths         = require('metalsmith-paths')
const dateFormatter = require('metalsmith-date-formatter')
const markdown      = require('metalsmith-markdown')
const child_process = require("child_process")

Metalsmith(__dirname)

  // add global filepath variable
  .metadata({
    noteasyFilepath: ('file://' + __dirname)
  })

  .source('./source')
  .destination('./build')
  .clean(true)

  // Add 'collection: notes' to every note .md file + create a 'notes' metaobject about files.
  .use(collections({
    notes: {
      pattern: '**/*.md',
      sortBy: 'date',
      reverse: true 
    }
  }))
   
   // Prettify date format
   // Uses 'moment' date formats: http://momentjs.com/
  .use(dateFormatter({
    dates: [
      {
        key: 'date',
        format: 'Do MMM YYYY' // e.g. 27th Nov 2018
      }
    ]
  }))
  
  // Transpile all markdown files to html
  // (This uses metalsmith-markdown which seems excessive since inplace could do it, but inplace just doesn't render everything so good :/)
  .use(markdown({
    pattern: "**/*.md"
  }))

  // Adds 'paths' properties. 
  // Keeps original 'path' (i.e. 'file.md') intact, allowing it be used for linking the original md file (for editing) and the built html file.
  .use(paths({ 
    property: "paths"
  }))

  // Create index page for each tag. n.b.: turn tags into [name, slug] arrays!
  .use(tags({
    layout: 'index_tagX.njk',
    metadataKey: "globalTags",    
  }))
   
  // Apply templates to files
  // Default is 'note.njk', unless overriden by 'layout: x.njl' YFM
  .use(layouts({
    pattern: ['**'],
    default: "note.njk"
  }))

  // 
  .use(inplace())

  //.use(writemetadata({            // write the JS object
    //pattern: ['**/*'],            // for each file into .json
    //ignorekeys: ['next', 'previous'],
    //bufferencoding: 'utf8',        // also put 'content' into .json  
    //collections: {
      //notes: {
        //output: {
          //path: 'notes.json',
          //asObject: true,
          //metadata: {
            //"type": "list"
          //}
        //}
      //}
    //}
  //}))
  
  //.use(debug({
    //metadata: true,         // default: true
    //source: false,           // default: true
    //destination: false,      // default: true
    //files: true,             // default: true
    //match: "**/*.md"         // default: all files  
  //}))

  .build((err) => {
    if (err) { // error handling is required
      throw err 
    } else {
      console.log('✓ Build successful');
      // execute shell script to refresh noteviewer in browser
      try {
        child_process.execSync(__dirname + '/assets/refreshFirstPinnedFirefoxTab.sh');
        console.log('First pinned Firefox tab refreshed');    
      }
      catch(error) {
        console.log('is /assets/refreshFirstPinnedFirefoxTab.sh executable?');
      }
    }
  })
