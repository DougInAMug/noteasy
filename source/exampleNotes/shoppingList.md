---
date: 2019-01-15
title: Shopping list
tags: exampleTag_expenditure
collection: lists
---

- Kale
- Coconut cream
- Cox Pippin Apples
- Rye flour
- Plain soap
- Parsley
- Jerusalem artichoke
